# Workload replay

_**Integrated in [FaaSLoad](https://gitlab.com/faasload/faasload)** as a [helper script (`scripts/azure-trace-converter.py`)](https://gitlab.com/faasload/faasload/-/blob/fdc8fea6077e079680b9d1c133f000e3fda494f6/scripts/azure-trace-converter.py)._

*Wissam OUALI & Aymeric FÜLÖP*

Based on Azure datasets, this project aims to create realistic Function as a Service (FaaS) workloads in order to replay them in FaasLoad.

## Table of Contents

- [Content](#content)
- [Links](#links)

## Content

- `sample.ipynb`: run this notebook if you want to generate traces in `.faas` format.
- `injection-traces`: generated traces in `.faas` format using the `go_dummy` function, separated between the top 1% invocated functions and the remaining ones.
- `graph-generation.ipynb`: work related with the function graph generation.
- `Azure-dataset-report.pdf`: small report concerning the Azure Dataset and the ORCBench methodology (which one we applied in `sample.ipynb`) and other considerations made at the early stage of the project

## Links

- [Azure Function Trace 2019](https://github.com/Azure/AzurePublicDataset/blob/master/AzureFunctionsDataset2019.md)
- [FaasLoad](https://gitlab.com/faasload/faasload)
- [FaasLoad Platform Configuration](https://gitlab.com/faasload/platform-configurations/-/tree/main)
- [Cloud Programming Simplified: A Berkeley View on
Serverless Computing](https://www2.eecs.berkeley.edu/Pubs/TechRpts/2019/EECS-2019-3.pdf)
- [Generating Complex, Realistic Cloud Workloads using Recurrent Neural Networks](https://dl.acm.org/doi/abs/10.1145/3477132.3483590)
- [OrcBench: A Representative Serverless Benchmark](https://rcs.uwaterloo.ca/~ryan/files/orcbench.pdf)
